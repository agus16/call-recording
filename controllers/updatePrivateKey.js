const { formData, fileSystem } = require('../helpers')
const fs = require('fs')
const { DECRYPT_PRIVATEKEY_PATH } = process.env
const shell = require('shelljs');
module.exports = async (req, res) => {
    try {
        const result = await formData(req)
        if (!result) {
            return Respon.fail(500, result, res)
        }
        if (!result.files.file) {
            return res.status(400).json("Format Failed")
        }

        const resultBody = await fileSystem.readFile(result.files.file.filepath)

        const createFile = await fileSystem.writeFile(DECRYPT_PRIVATEKEY_PATH, resultBody)

        console.log(createFile, "<=== Upload PrivateKey success")

        res.json({ message: "Upload PrivateKey Success" })

        return shell.exec("pm2 reload ecosystem.config.js --force")
    } catch (error) {
        console.log(error)
        return res.status(500).json(error.name)
    }
}