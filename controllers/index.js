const callRecording = require("./callRecording")
const health = require("./health")
const updateEnv = require("./updateEnv")
const updatePrivateKey = require("./updatePrivateKey")
const downloadCallRecording = require("./downloadCallRecording")
module.exports = {
    health,
    callRecording,
    updateEnv,
    updatePrivateKey,
    downloadCallRecording
}