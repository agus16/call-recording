const Azure = require("../helpers/store/Azure")
const crypto = require("crypto");
const env = require('../helpers/env')
const fs = require('fs')
const moment = require('moment')
// const { STORAGE_LOCAL_FOLDER } = process.env
module.exports = async (req, res) => {
    try {
        const Env = await env()
        const { STORAGE_TYPE, STORAGE_AZURE_STORAGE_TYPE, STORAGE_LOCAL_FOLDER } = Env
        const fileName = req.params.fileName
        let result
        let rows
        if (fileName && fileName.indexOf('_') !== -1) {
            rows = fileName.split('_')
        }
        const Filedir = moment(rows.shift()).format('YYYY-MM-DD')
        if (!STORAGE_TYPE) {
            return res.status(404).json("Env Not Value")
        }
        if (STORAGE_TYPE === "local") {
            result = await fs.readFileSync(`${STORAGE_LOCAL_FOLDER}/recordings/${Filedir}/${fileName}`)
        } else if (STORAGE_TYPE === "azure") {
            if (!STORAGE_AZURE_STORAGE_TYPE) {
                return res.status(404).json("Env Not Value")
            }
            if (STORAGE_AZURE_STORAGE_TYPE === "container") {

            } else if (STORAGE_AZURE_STORAGE_TYPE === "share") {
                result = await Azure.downloadFileShare(fileName)
            }
        }

        if (!result) {
            return res.json(result)
        }
        const digest = crypto.createHash("sha256").update(result).digest('base64')
        console.log(digest)
        res.setHeader('Digest', `sha256=${digest}`);
        res.setHeader('Content-Type', "audio/mpeg");
        res.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
        res.send(result);
    } catch (error) {
        console.log(error, "error")
        if (error.name) {
            return res.status(500).json(error.name)
        }
        return res.status(404).json(error)
    }
}