const env = require('../helpers/env')
const { DECRYPT_PRIVATEKEY_PATH } = process.env
const fs = require('fs')
module.exports = async (req, res) => {
    try {
        const Env = await env()
        const {
            SECRET,
            API_URL,
            DECRYPT_ENABLED,            
            STORAGE_TYPE,
            STORAGE_LOCAL_FOLDER,
            STORAGE_AZURE_CONNECTION_STRING,
            STORAGE_AZURE_STORAGE_TYPE,
            STORAGE_AZURE_STORAGE_NAME,
        } = Env

        if (DECRYPT_ENABLED === true) {
            const file = fs.readFileSync(DECRYPT_PRIVATEKEY_PATH, 'utf-8')
            if (file === "") {
                return res.status(403).json({ message: "Event Call Recording Not Running , PrivateKey Not Value" })
            }
        }
        if (!SECRET || !API_URL || !DECRYPT_ENABLED || !STORAGE_TYPE  ) {
            return res.status(403).json({ message: "Event Call Recording Not Running , Env Not Value" })
        }
        if (STORAGE_TYPE === "local") {
            if (!STORAGE_LOCAL_FOLDER) {
                return res.status(403).json({ message: "Event Call Recording Not Running , STORAGE_LOCAL_FOLDER Not Value" })
            }
        } else if (STORAGE_TYPE === "azure") {
            if (!STORAGE_AZURE_CONNECTION_STRING || !STORAGE_AZURE_STORAGE_TYPE || !STORAGE_AZURE_STORAGE_NAME) {
                return res.status(403).json({ message: "Event Call Recording Not Running , STORAGE_AZURE Not Value" })
            }
        }

        return res.json({ message: "Event Call Recording Running" })
    } catch (error) {
        return res.status(403).json({ message: "Event Call Recording Not Running" })

    }
}