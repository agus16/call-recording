
const EventEmitter = require('events');
const { Api } = require('../function');
const editJsonFile = require("edit-json-file");

// const { SECRET, DECRYPT_ENABLED, STORAGE_TYPE, SOURCE_DELETE_ON_SUCCESS_ENABLED } = process.env
const ReadableStreamClone = require("readable-stream-clone");
const { checkSum, storage, decryptFile } = require('../helpers');
// const store = require('../helpers/store');

const env = require('../helpers/env')
const axios = require('axios')

function waitforme(ms) {
    return new Promise(resolve => { setTimeout(resolve, ms); });
}

class callRecording extends EventEmitter {
    constructor() {
        super();
        this.cursor = "earliest";
        this.digest = ""
        this.fileKey = "";
        this.state = false
        this.backCursor = ""
        this.txnUuid = ""
        this.length = 0
    }
    async start() {
        this.emit("start")
    }
    async init() {
        console.log("Start <=== Schedule")
        const Env = await env()
        const { CURSOR, SECRET, DECRYPT_ENABLED, STORAGE_TYPE, SOURCE_DELETE_ON_SUCCESS_ENABLED, Length } = Env
        let resultApiCursor
        let resultCallRecording
        let cloneStream1
        let cloneStream2
        let cloneStream3
        let resultCheckSum
        let resultDecrypt
        let resultStorage
        let resultRemove
        let fileName
        try {
            console.log("Get Cursor :", CURSOR || this.cursor)
            this.backCursor = CURSOR || this.cursor
            try {
                resultApiCursor = await Api.get(`/_o/v3/callRecordingEvent/${CURSOR || this.cursor}`, { secret: SECRET })
            } catch (error) {
                console.log(error, "resultApiCursor")
                if (error.response) {
                    const { status, data } = error.response
                    console.log(status, "<=== status")
                    console.log(data, "<=== data")
                    console.log("<====  Error Get Cursor")
                    return this.emit("done")
                }
            }
            if (resultApiCursor.data.data.length) {
                let file = editJsonFile(`env.json`);
                const rows = resultApiCursor.data.data
                if (rows.length === 1000) {
                    console.log("next cursor :", resultApiCursor.data.nextCursor)
                    this.cursor = resultApiCursor.data.nextCursoriyah
                    file.set("Length", 0);
                    file.save();
                    file = editJsonFile(`env.json`, {
                        autosave: true
                    })
                } else {
                    file.set("CURSOR", this.backCursor);
                    // file.set("Length", Length);
                    file.save();
                    file = editJsonFile(`env.json`, {
                        autosave: true
                    })
                }
                for (let i = 0; i < rows.length; i++) {
                    await waitforme(1000);
                    try {
                        console.log(`<=== Total Data Call Recording Length ${rows.length} ===>`)
                        if (i >= Length) {
                            console.log(`<=== Data Length ${i + 1} Proccess ===>`)
                            this.fileKey = rows[i].fileKey;
                            try {
                                // donwload Call recording
                                resultCallRecording = await Api.getStream(`/_o/v2/files/${this.fileKey}`, { secret: SECRET })
                            } catch (error) {
                                if (error.response) {
                                    let streamString = ''
                                    error.response.data.setEncoding('utf8')
                                    error.response.data
                                        .on('data', (utf8Chunk) => { streamString += utf8Chunk })
                                        .on('end', () => {
                                            console.log(streamString, "<==== error Download File")
                                            console.log(this.fileKey, "<==== error Download File")
                                        })
                                }
                                continue;
                            }
                            this.digest = resultCallRecording.headers.digest
                            cloneStream1 = new ReadableStreamClone(resultCallRecording.data);
                            cloneStream2 = new ReadableStreamClone(resultCallRecording.data);
                            cloneStream3 = new ReadableStreamClone(resultCallRecording.data);
                            // //  Check Sum  download
                            console.log("<=== Process Checksum ===>")
                            resultCheckSum = await checkSum(this.digest, cloneStream1)
                            if (resultCheckSum) {
                                console.log("<=== success Checksum ===>")
                                if (resultCallRecording.headers["content-type"] === "application/pgp-encrypted") {
                                    //   decrypt File
                                    console.log("<=== Process decrypt File  ===>")
                                    if (DECRYPT_ENABLED === true) {
                                        try {
                                            resultDecrypt = await decryptFile(cloneStream2)
                                            console.log("<=== success Decrypt ===>")
                                            console.log("<=== decrypt File Successfull ===>")
                                            fileName = resultDecrypt.filename
                                            try {
                                                resultStorage = await storage(this.fileKey, resultDecrypt.data)
                                            } catch (error) {
                                                console.log(error, `<=== Error Save FIle To ${STORAGE_TYPE} ===>`)
                                                console.log(`<=== Save File To ${STORAGE_TYPE} Failed ===>`)
                                                file.set("CURSOR", this.backCursor);
                                                file.set("Length", i === 0 ? 0 : i - 1);
                                                this.length = i === 0 ? 0 : i - 1
                                                file.save();
                                                file = editJsonFile(`env.json`, {
                                                    autosave: true
                                                })
                                                this.emit("done")
                                                break;
                                            }
                                            // save file
                                        } catch (error) {
                                            console.log(error, "<=== Error Decrypt ===>")
                                            console.log("<=== decrypt File Failed ===>")
                                            this.cursor = this.backCursor
                                            console.log(CURSOR || this.cursor, "<=== Back Cursor")
                                            file.set("Length", i === 0 ? 0 : i - 1);
                                            file.set("CURSOR", this.backCursor);
                                            file.save();
                                            file = editJsonFile(`env.json`, {
                                                autosave: true
                                            })
                                            this.emit("done")
                                            break;
                                        }
                                    } else {
                                        // save file
                                        try {
                                            console.log("<=== Save FIle Not decrypt  ===>")
                                            resultStorage = await storage(this.fileKey, cloneStream3)
                                        } catch (error) {
                                            console.log(error, `<=== Error Save FIle To ${STORAGE_TYPE} ===>`)
                                            console.log(`<=== Save File To ${STORAGE_TYPE} Failed ===>`)
                                            file.set("CURSOR", this.backCursor);
                                            file.set("Length", i === 0 ? 0 : i - 1);
                                            this.length = i === 0 ? 0 : i - 1
                                            file.save();
                                            file = editJsonFile(`env.json`, {
                                                autosave: true
                                            })
                                            this.emit("done")
                                            break;
                                        }
                                    }
                                } else {
                                    // save file
                                    try {
                                        console.log("<=== Save FIle Not decrypt  ===>")
                                        resultStorage = await storage(this.fileKey, cloneStream3)
                                    } catch (error) {
                                        console.log(error, `<=== Error Save FIle To ${STORAGE_TYPE} ===>`)
                                        console.log(`<=== Save File To ${STORAGE_TYPE} Failed ===>`)
                                        file.set("CURSOR", this.backCursor);
                                        file.set("Length", i === 0 ? 0 : i - 1);
                                        this.length = i === 0 ? 0 : i - 1
                                        file.save();
                                        file = editJsonFile(`env.json`, {
                                            autosave: true
                                        })
                                        this.emit("done")
                                        break;
                                    }
                                }
                                if (resultStorage) {
                                    console.log(`<=== Save File To ${STORAGE_TYPE} Successfull ===>`)
                                    file.set("Length", i);
                                    this.length = i
                                    file.save();
                                    file = editJsonFile(`env.json`, {
                                        autosave: true
                                    })
                                    continue;
                                } else {
                                    console.log(`<=== Save File To ${STORAGE_TYPE} Failed ===>`)
                                    file.set("CURSOR", this.backCursor);
                                    file.set("Length", i === 0 ? 0 : i - 1);
                                    this.length = i === 0 ? 0 : i - 1
                                    file.save();
                                    file = editJsonFile(`env.json`, {
                                        autosave: true
                                    })
                                    this.emit("done")
                                    break;
                                }
                            } else {
                                console.log("<=== Checksum Failed ===>")
                                this.cursor = this.backCursor
                                console.log(CURSOR || this.cursor, "<=== Back Cursor")
                                file.set("CURSOR", this.backCursor);
                                file.set("Length", i === 0 ? 0 : i - 1);
                                this.length = i === 0 ? 0 : i - 1
                                file.save();
                                file = editJsonFile(`env.json`, {
                                    autosave: true
                                })
                                this.emit("done")
                                break;
                            }
                        } else {
                            console.log(rows[i].fileKey, "<=== File Already Storage")
                            console.log(`<=== Data Length ${i + 1} Not Proccess ===>`)
                            this.length = i
                            file.set("Length", i);
                            file.save();
                            file = editJsonFile(`env.json`, {
                                autosave: true
                            })
                            continue;
                        }
                    } catch (error) {
                        console.log("<=== End Schedule ===>")
                        this.emit("done")
                        break;
                    }
                }

                if (rows.length === 1000) {
                    file.set("CURSOR", resultApiCursor.data.nextCursor);
                    file.set("Length", 0);
                    console.log(`<==== End Length ${rows.length} ====>`)
                } else {
                    console.log(`<==== End Length ${this.length + 1} ====>`)
                    file.set("Length", this.length + 1);
                }
                file.save();
                file = editJsonFile(`env.json`, {
                    autosave: true
                })
                console.log("<=== End Schedule ===>")
                return this.emit("done")
            } else {
                console.log("Data Not Found")
                this.cursor = this.backCursor
                console.log(CURSOR || this.cursor, "<=== Back Cursor")
                file.set("CURSOR", this.backCursor);
                file.set("Length", Length);
                file.save();
                file = editJsonFile(`env.json`, {
                    autosave: true
                })
                console.log("<=== End Schedule ===>")
                return this.emit("done")
            }
        } catch (error) {
            console.log(error)
            console.log("<=== End Schedule ===>")
            return this.emit("done")
        }
    }
}
module.exports = callRecording