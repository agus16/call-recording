const editJsonFile = require("edit-json-file");
const Validator = require('fastest-validator');
const v = new Validator();
const shell = require('shelljs');
module.exports = async (req, res) => {
  try {
    const schema = {
      SECRET: 'string|empty:false',
      API_URL: 'string|empty:false',
      DECRYPT_ENABLED: 'boolean|empty:false',
      DECRYPT_PASSPHRASE: 'string|empty:false',
      STORAGE_TYPE: { type: "enum", values: ["local", "azure"] },
      SOURCE_DELETE_ON_SUCCESS_ENABLED: 'boolean|empty:false',
      TIME: 'string|empty:false'
    }
    const validate = v.validate(req.body, schema);
    if (validate.length) {
      return res.status(400).json({ message: validate });
    }
    if (req.body.STORAGE_TYPE === "azure") {
      const schemaAzure = {
        STORAGE_AZURE_CONNECTION_STRING: 'string|empty:false',
        STORAGE_AZURE_STORAGE_TYPE: { type: "enum", values: ["container", "share"] },
        STORAGE_AZURE_STORAGE_NAME: 'string|empty:false',
      }
      const validate = v.validate(req.body, schemaAzure);
      if (validate.length) {
        return res.status(400).json({ message: validate });
      }
    } else if (req.body.STORAGE_TYPE === "local") {
      const schemaLocal = {
        STORAGE_LOCAL_FOLDER: 'string|empty:false',
      }
      const validate = v.validate(req.body, schemaLocal);
      if (validate.length) {
        return res.status(400).json({ message: validate });
      }
    }


    let file = editJsonFile(`env.json`);
    const key = Object.keys(req.body)
    const keyFile = Object.keys(file.data)
    keyFile.forEach((ee) => {
      key.forEach(e => {
        if (ee === e) {
          file.set(e, req.body[e]);
        }
      })
    });
    file.save();
    file = editJsonFile(`env.json`, {
      autosave: true
    })
    console.log(file.data, "updaate Env")
    res.json({ message: "Update Env Success" })

    return shell.exec("pm2 reload ecosystem.config.js --force")
  } catch (error) {
    console.log(error)
    return res.status(500).json(error.name)
  }
}