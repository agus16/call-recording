
const EventEmitter = require('events');
const { Api } = require('../function');
const editJsonFile = require("edit-json-file");

// const { SECRET, DECRYPT_ENABLED, STORAGE_TYPE, SOURCE_DELETE_ON_SUCCESS_ENABLED } = process.env
const ReadableStreamClone = require("readable-stream-clone");
const { checkSum, storage, decryptFile } = require('../helpers');
// const store = require('../helpers/store');

const env = require('../helpers/env')
const axios = require('axios')
class callRecording extends EventEmitter {
    constructor() {
        super();
        this.cursor = "earliest";
        this.digest = ""
        this.fileKey = "";
        this.state = false
        this.backCursor = ""
    }
    async start() {
        this.emit("start")
    }
    async init() {
        console.log("Start <=== Schedule")
        const Env = await env()
        const { CURSOR, SECRET, DECRYPT_ENABLED, STORAGE_TYPE, SOURCE_DELETE_ON_SUCCESS_ENABLED } = Env
        let resultApiCursor
        let resultCallRecording
        let cloneStream1
        let cloneStream2
        let cloneStream3
        let resultCheckSum
        let resultDecrypt
        let resultStorage
        let resultRemove
        let fileName
        try {

            console.log("Get Cursor :", CURSOR || this.cursor)
            this.backCursor = CURSOR || this.cursor
            try {
                resultApiCursor = await Api.get(`/_o/v2/callRecordingEvent/${CURSOR || this.cursor}`, { secret: SECRET })
            } catch (error) {
                console.log(error)
                if (error.response) {
                    const { status, data } = error.response
                    console.log(status, "<=== status")
                    console.log(data, "<=== data")
                    console.log("<====  Error Get Cursor")
                    return this.emit("done")
                }
            }
            if (resultApiCursor) {
                if (resultApiCursor.data.data !== null) {
                    console.log("next cursor :", resultApiCursor.data.nextCursor)
                    this.cursor = resultApiCursor.data.nextCursor
                    // save cursor to env.json
                    let file = editJsonFile(`env.json`);
                    file.set("CURSOR", resultApiCursor.data.nextCursor);
                    file.save();
                    file = editJsonFile(`env.json`, {
                        autosave: true
                    })
                    // download call Recording
                    if (resultApiCursor.data.data) {
                        this.fileKey = resultApiCursor.data.data.fileKey
                        try {
                            resultCallRecording = await Api.getStream(`/_o/v2/files/${this.fileKey}`, { secret: SECRET })
                        } catch (error) {
                            if (error.response) {
                                let streamString = ''
                                error.response.data.setEncoding('utf8')
                                error.response.data
                                    .on('data', (utf8Chunk) => { streamString += utf8Chunk })
                                    .on('end', () => {
                                        console.log(streamString, "<==== error Download File")
                                        console.log("End <=== Schedule")
                                    })
                                return this.emit("done")
                            }
                        }
                    }
                } else {
                    console.log("Data Not Found")
                    console.log("End <=== Schedule")
                    this.emit("done")
                }
            } 
            if (resultCallRecording) {
                this.digest = resultCallRecording.headers.digest;

                cloneStream1 = new ReadableStreamClone(resultCallRecording.data);
                cloneStream2 = new ReadableStreamClone(resultCallRecording.data);
                cloneStream3 = new ReadableStreamClone(resultCallRecording.data);
                // // // Check Sum 
                resultCheckSum = await checkSum(this.digest, cloneStream1)
                if (!resultCheckSum) {
                    console.log("Checksum failed")
                    this.cursor = this.backCursor
                    console.log(CURSOR || this.cursor, "<=== Back Cursor")
                    // save cursor to env.json
                    let file = editJsonFile(`env.json`);
                    file.set("CURSOR", this.backCursor);
                    file.save();
                    file = editJsonFile(`env.json`, {
                        autosave: true
                    })
                    console.log("End <=== Schedule")
                    this.emit("done")
                } else {
                    console.log("Checksum Successfull")
                }
                // decrypt File
                if (DECRYPT_ENABLED === true) {
                    resultDecrypt = await decryptFile(cloneStream2)
                    if (resultDecrypt) {
                        console.log("decrypt File Successfull")
                        fileName = resultDecrypt.filename
                        // push to storage
                        resultStorage = await storage(this.fileKey, resultDecrypt.data)
                    } else {
                        console.log("decrypt File Failed")
                        this.cursor = this.backCursor
                        console.log(CURSOR || this.cursor, "<=== Back Cursor")
                        // save cursor to env.json
                        let file = editJsonFile(`env.json`);
                        file.set("CURSOR", this.backCursor);
                        file.save();
                        file = editJsonFile(`env.json`, {
                            autosave: true
                        })
                        console.log("End <=== Schedule")
                        this.emit("done")
                    }
                } else {
                    const header = resultCallRecording.headers['content-disposition']
                    if (header) {
                        if (header.indexOf("filename=")) {
                            fileName = header.split('filename=')[1]
                        }
                    }
                    // push to storage
                    resultStorage = await storage(this.fileKey, cloneStream3)
                }

                // save storage 
                if (resultStorage) {
                    console.log(`Save File To ${STORAGE_TYPE} Successfull`)
                    // remove Call Recording
                    if (SOURCE_DELETE_ON_SUCCESS_ENABLED === true) {
                        resultRemove = await Api.get(`/_o/v2/files/${this.fileKey}?secret=${SECRET}`)
                        if (resultRemove) {
                            console.log("Remove File Call Recording")
                            console.log("End <=== Schedule")
                            this.emit("done")
                        }
                    } else {
                        console.log("End <=== Schedule")
                        this.emit("done")
                    }
                } else {
                    console.log(`Save File To ${STORAGE_TYPE} Failed`)
                    console.log("End <=== Schedule")
                    this.emit("done")
                }
            }
        } catch (error) {
            if (error.response) {
                const { status, data } = error.response
                console.log(status, "<=== status")
                console.log(data, "<=== data")
                console.log("End <=== Schedule")
                return this.emit("done")

            }
            // FIle Not Encrypt
            if (error.message === "Misformed armored text") {
                console.log("file Not Encrypted")
                console.log(error.message, "<=== error.message")
                const header = resultCallRecording.headers['content-disposition']
                if (header) {
                    if (header.indexOf("filename=")) {
                        fileName = header.split('filename=')[1]
                    }
                }
                resultStorage = await storage(this.fileKey, cloneStream3)
                // save storage 
                if (resultStorage) {
                    console.log(`Save File To ${STORAGE_TYPE} Successfull`)
                    // remove Call Recording
                    if (SOURCE_DELETE_ON_SUCCESS_ENABLED === true) {
                        resultRemove = await Api.get(`/_o/v2/files/${this.fileKey}?secret=${SECRET}`)
                        if (resultRemove) {
                            console.log("Remove File Call Recording")
                            console.log("End <=== Schedule")
                            return this.emit("done")
                        }
                    } else {
                        console.log("End <=== Schedule")
                        return this.emit("done")
                    }
                } else {
                    console.log(`Save File To ${STORAGE_TYPE} Failed`)
                    return this.emit("done")
                }
            }
            if (error.message === "Incorrect key passphrase") {
                console.log(error.message, "<=== error.message")
                this.cursor = this.backCursor
                console.log(CURSOR || this.cursor, "<=== Back Cursor")
                // save cursor to env.json
                let file = editJsonFile(`env.json`);
                file.set("CURSOR", this.backCursor);
                file.save();
                file = editJsonFile(`env.json`, {
                    autosave: true
                })
                console.log("End <=== Schedule")
                return this.emit("done")
            }
            console.log(error.message, "<==== error")
            this.cursor = this.backCursor
            console.log(CURSOR || this.cursor, "<=== Back Cursor")
            // save cursor to env.json
            let file = editJsonFile(`env.json`);
            file.set("CURSOR", this.backCursor);
            file.save();
            file = editJsonFile(`env.json`, {
                autosave: true
            })
            console.log("End <=== Schedule")
            return this.emit("done")
        }
    }
}
module.exports = callRecording