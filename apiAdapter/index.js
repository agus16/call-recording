const axios = require('axios');
const https = require('https');
module.exports = (baseUrl) => {
    return axios.create({
        baseURL: baseUrl,
        httpsAgent: new https.Agent({
            rejectUnauthorized: false
        })
    });
}
