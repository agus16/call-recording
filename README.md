<!-- Run only 1 copy of this program  -->

There is no benefit in running more than 1 copy of the program configured with the same storage endpoint. In fact, doing so can potentially corrupt the files. The run script will attempt to detect and prevent the same program from running in the same system but it can't protect against programs running from different systems.

<!-- File config  -->

1. Edit .env properties to configure
   - SECRET
   - API_URL
   - DECRYPT_PRIVATEKEY_PATH , private key file name example the name private key "examplePrivateKey.asc", your update at "decrypt.privateKey.path=./config/examplePrivateKey.asc"
   - DECRYPT_PASSPHRAS
   - STORAGE_AZURE_CONNECTION_STRING
   - STORAGE_AZURE_CONTAINER

please your complate the requirements

<!-- How To set up To docker -->
1. build docker file
<!--  Build docker file -->
docker build --tag <dockerHubName/Apps:version> .
<!-- example -->
docker build --tag agussyaf0825/call-recording:1.0 .
<!-- End Build docker file -->

2. push docker to registry dockerHUb
<!-- optional -->
<!--  Docker Push To Registry dockerHub -->
docker push <dockerHubName/Apps:version>
<!-- example -->
docker push agussyaf0825/call-recording:1.0
<!-- End Docker Push To Registry dockerHub -->

3. Optional Run the docker
<!-- optional -->
<!--  docker run  -->
docker run -d --name call-recording -p 9393:9393 agussyaf0825/call-recording:1.0
<!-- End docker run  -->

4. push the docker to kubernetes
<!-- example kubernet -->
kubectl apply -f call-recording.yaml

<!-- Config Kubernet set up by Default -->
1. pod default connection internet
2. pod not recommended auto scaling

<!-- StartUp in Pm2-->
1. sudo pm2 save
2. sudo pm2 startup
3. sudo pm2 save
4. sudo systemctl status pm2-root


<!-- StartUp in Shell Script-->
1. mkdir /usr/bin/pm2-startup/
2. cp start.sh /usr/bin/pm2-startup/
3. chmod 777 -R /usr/bin/pm2-startup/start.sh
4. cp call-recording.service /etc/systemd/system/
5. chmod 777 -R /etc/systemd/system/call-recording.service
6. sudo systemctl enable call-recording
7. sudo systemctl start call-recording
8. sudo systemctl status call-recording


