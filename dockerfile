FROM node:12-alpine

RUN apk --no-cache add make git gcc libtool musl-dev ca-certificates dumb-init 
RUN apk add -U tzdata
RUN cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

RUN npm install pm2 -g
RUN mkdir /app
WORKDIR /app
ADD . /app/
RUN npm install

EXPOSE 9393

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]




# build == docker build --tag service-users:1.0 . ==

# run == docker container create --name service-users -p 8086 :8086 service-users:1.0


#duplikat images docker tag service-users:1.0 agussyaf0825/service-users:1.0