const Promise = require('bluebird')
const {
    API_URL,
} = process.env;

const apiAdapeter = require('../apiAdapter')
// const api = apiAdapeter(API_URL)
const { env } = require('../helpers')

class Api {
    static async get(url, urlParams) {
        return new Promise(async function (resolve, reject) {
            try {
                const Env = await env()
                const { API_URL } = Env
                const api = apiAdapeter(API_URL)
                const result = await api.get(`${url}`, {
                    params: urlParams
                })
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }
    static async getStream(url, urlParams) {
        return new Promise(async function (resolve, reject) {
            try {
                const Env = await env()
                const { API_URL } = Env
                const api = apiAdapeter(API_URL)
                const result = await api.get(`${url}`, {
                    params: urlParams,
                    responseType: "stream"
                })
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }
    static async post(url, data) {
        return new Promise(async function (resolve, reject) {
            try {
                const Env = await env()
                const { API_URL } = Env
                const api = apiAdapeter(API_URL)
                const result = await api.post(`${url}`, data)
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }
    static async put(url, data) {
        return new Promise(async function (resolve, reject) {
            try {
                const Env = await env()
                const { API_URL } = Env
                const api = apiAdapeter(API_URL)
                const result = await api.put(`${url}`, data)
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }
    static async delete(url, data) {
        return new Promise(async function (resolve, reject) {
            try {
                const Env = await env()
                const { API_URL } = Env
                const api = apiAdapeter(API_URL)
                const result = await api.delete(`${url}`, {
                    data
                })
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }
}
module.exports = Api