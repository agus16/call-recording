const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  const data = {
    success: true,
    message: "Service-Call-Recording Active",
  };
  return res.json(data).end();
});

module.exports = router;
