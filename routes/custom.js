const express = require('express');
const router = express.Router();
const update = require('../controllers')


router.post('/env', update.updateEnv);
router.post('/privateKey', update.updatePrivateKey);

module.exports = router;
