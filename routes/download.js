const express = require('express');
const router = express.Router();
const download = require('../controllers')


router.get('/recordings/download/:fileName', download.downloadCallRecording);

module.exports = router;
