
const {
    ACCESS_API,
} = process.env;

module.exports = (req, res, next) => {
    try {
        if (ACCESS_API !== req.query.secret) {
            return res.status(401).json("Api-key invalid")
        } else {
            next()
        }
    } catch (error) {
        return res.status(401).json("Api-key invalid")
    }
}