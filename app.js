require('dotenv').config();
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const PORT = process.env.PORT || 9595;
const indexRouter = require('./routes/index');
const app = express();
const CallRecording = require('./controllers/callRecording')
const callRecording = new CallRecording()
const customRouter = require('./routes/custom')
const healthRouter = require('./routes/health')
const downloadRouter = require('./routes/download')
const env = require('./helpers/env');
const { verifyApiKeyAcess } = require('./middleware');
const { DECRYPT_PRIVATEKEY_PATH } = process.env
const fs = require('fs');
const { decryptFile } = require('./helpers');
const moment = require('moment')
require('moment-timezone')
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/', indexRouter);
app.use('/api/v1', verifyApiKeyAcess, customRouter);
app.use('/api/v1', verifyApiKeyAcess, healthRouter);
// app.use('/api/v1', verifyApiKeyAcess, downloadRouter);



let state = false
const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
callRecording.on('done', () => {
    console.log(`<==== Time Now ${moment().tz(timezone).format('YYYY-MM-DD HH:mm:ss')} ====>`)
    env().then(({ TIME }) =>
    console.log(`<==== Waiting Start Schedule ${moment().tz(timezone).add(TIME || 5 * 60 * 1000, 'milliseconds').format('YYYY-MM-DD HH:mm:ss')} ====>`
    ))
    state = true;
});
callRecording.on('start', () => {
    console.log(`<==== Time Now ${moment().tz(timezone).format('YYYY-MM-DD HH:mm:ss')} ====>`)
    env().then(({ TIME }) =>
        console.log(`<==== Waiting Start Schedule ${moment().tz(timezone).add(TIME || 5 * 60 * 1000, 'milliseconds').format('YYYY-MM-DD HH:mm:ss')} ====>`
        ))
    state = true;
});

async function main() {
    const Env = await env()
    const {
        SECRET,
        API_URL,
        DECRYPT_ENABLED,
        STORAGE_TYPE,
        STORAGE_LOCAL_FOLDER,
        STORAGE_AZURE_CONNECTION_STRING,
        STORAGE_AZURE_STORAGE_TYPE,
        STORAGE_AZURE_STORAGE_NAME,
        TIME,
    } = Env

    console.log("Call Recording Running");
    console.log(state, "<=== state")
    setInterval(async function () {
        console.log(state, "<=== state")
        if (SECRET && API_URL && STORAGE_TYPE) {
            if (state) {
                // if (STORAGE_TYPE === "local") {
                //     if (!STORAGE_LOCAL_FOLDER) {
                //         state = false
                //         console.log(`STORAGE_TYPE : ${STORAGE_TYPE}, STORAGE_LOCAL_FOLDER Not Value`)
                //         console.log("Event Call Recording Not START")
                //         console.log(state, "<=== state")
                //         return await callRecording.start();
                //     }
                // } else if (STORAGE_TYPE === "azure") {
                //     if (!STORAGE_AZURE_CONNECTION_STRING && !STORAGE_AZURE_STORAGE_TYPE && !STORAGE_AZURE_STORAGE_NAME) {
                //         state = false
                //         console.log(`STORAGE_TYPE : ${STORAGE_TYPE}, STORAGE_AZURE Not Value`)
                //         console.log("Event Call Recording Not START")
                //         console.log(state, "<=== state")
                //         return await callRecording.start();
                //     }
                // }
                // if (DECRYPT_ENABLED === true) {
                //     const file = await fs.readFileSync(DECRYPT_PRIVATEKEY_PATH, 'utf-8')
                //     if (file === "") {
                //         state = false
                //         console.log(`DECRYPT_ENABLED : ${DECRYPT_ENABLED}, PrivateKey Not Value`)
                //         console.log("Event Call Recording Not START")
                //         console.log(state, "<=== state")
                //         return await callRecording.start();
                //     }
                // }
                console.log("<==== Start Schedule ====>")
                state = false
                await callRecording.init();
            }
        } else {
            state = false
            console.log("Event Call Recording Not START")
            console.log(state, "<=== state")
            await callRecording.start();
        }
    }, (TIME || 5 * 60 * 1000));
    await callRecording.start();
}

main()
// // ===============================
app.listen(PORT, () => {
    console.log("Server Running On Port: ", PORT);
});
// // ===============================
