module.exports = {
    apps: [{
        name: "call-recording",
        script: "app.js",
        watch_delay: 1000,
        ignore_watch: ["node_modules"],
    }]
}