const fs = require('fs')
const Promise = require('bluebird')
async function env() {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await JSON.parse(fs.readFileSync('env.json').toString())
            resolve(result)
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}
module.exports = env