const crypto = require("crypto");
const Promise = require('bluebird')

module.exports = (digest, file) => {
    return new Promise((resolve, reject) => {
        file
            .pipe(crypto.createHash("sha256").setEncoding("base64"))
            .on("error", (err) => {
                reject(err);
            })
            .on("finish", function () {
                if (`sha256=${this.read("base64")}` === digest) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        
    });
};
