const { Local, Azure } = require('./store')
const Promise = require('bluebird')
const env = require('./env')

module.exports = async (fileName, file) => {
    return new Promise(async (resolve, reject) => {
        try {
            const Env = await env()
            const { STORAGE_TYPE, STORAGE_AZURE_STORAGE_TYPE } = Env
            let result
            switch (STORAGE_TYPE) {
                case 'local':
                    console.log(`Storage Type : ${STORAGE_TYPE} <=== Storage`)
                    result = await Local(fileName, file)
                    resolve(result)
                    break;
                case 'azure':
                    console.log(`Storage Type : ${STORAGE_TYPE} <=== Storage`)
                    if (STORAGE_AZURE_STORAGE_TYPE === "container") {
                        console.log("container")
                        result = await Azure.uploadContainer(fileName, file)
                        resolve(result)
                    } else if (STORAGE_AZURE_STORAGE_TYPE === "share") {
                        console.log("share")
                        result = await Azure.uploadFileShare(fileName, file)
                        resolve(result)
                    }
                    break;
                default:
                    break;
            }
        } catch (error) {
            reject(error)
        }
    })
}