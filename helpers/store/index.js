const Azure = require("./Azure");
const Local = require("./Local");

module.exports = {
    Azure,
    Local
}