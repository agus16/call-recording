const { BlobServiceClient } = require("@azure/storage-blob");
const { ShareServiceClient } = require("@azure/storage-file-share");
const Promise = require('bluebird')
const env = require('../../helpers/env')
const moment = require('moment')
const fs = require('fs')
class Azure {

    static async convertSteam(file) {
        return new Promise(async (resolve, reject) => {
            try {
                let chunks = []
                file.on("data", function (chunk) {
                    chunks.push(chunk)
                });
                file.on("end", function () {
                    resolve(Buffer.concat(chunks));
                });
            } catch (error) {
                reject(error)
            }
        })
    }

    static async uploadContainer(fileName, file) {
        return new Promise(async (resolve, reject) => {
            try {
                const Env = await env()
                const { STORAGE_AZURE_CONNECTION_STRING, STORAGE_AZURE_STORAGE_NAME } = Env

                const blobServiceClient = BlobServiceClient.fromConnectionString(STORAGE_AZURE_CONNECTION_STRING);
                const containerClient = blobServiceClient.getContainerClient(STORAGE_AZURE_STORAGE_NAME);
                const blockBlobClient = containerClient.getBlockBlobClient(fileName);
                const result = await blockBlobClient.uploadStream(file)
                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }

    static async uploadFileShare(fileName, file) {
        return new Promise(async (resolve, reject) => {
            try {
                const Env = await env()
                const { STORAGE_AZURE_CONNECTION_STRING, STORAGE_AZURE_STORAGE_NAME } = Env
                let rows
                if (fileName && fileName.indexOf('/') !== -1) {
                    rows = fileName.split('/')
                }

                const FileNames = rows.pop()
                // connection Azure
                const serviceClient = ShareServiceClient.fromConnectionString(STORAGE_AZURE_CONNECTION_STRING)

                // check shareClient
                const shareClient = serviceClient.getShareClient(STORAGE_AZURE_STORAGE_NAME)
                // create shareClient
                if (!(await shareClient.exists())) {
                    await shareClient.create();
                    console.log(`Create share ${shareName} successfully`);
                }

                // // check direktory
                const directoryName = rows.shift();
                const directoryClient = shareClient.getDirectoryClient(directoryName);
                // // create directory 
                if (! await directoryClient.exists()) {
                    await directoryClient.create();
                    console.log(`Create directory ${directoryName} successfully`);
                }

                // // check subDirecTory 
                const subDirectoryName = rows
                const subDirectoryClient = directoryClient.getDirectoryClient(subDirectoryName)
                if (!await subDirectoryClient.exists()) {
                    await subDirectoryClient.create()
                    console.log(`Create subDirectory ${subDirectoryName} successfully`);
                }

                const resultFile = await this.convertSteam(file)
                const content = resultFile.toString()
                const fileClient = subDirectoryClient.getFileClient(FileNames)

                await fileClient.create(content.length)
                const result = await fileClient.uploadData(resultFile, 0, content.length)

             
                console.log(`Upload To Azure File Share Success`);
                resolve(`Upload To Azure File Shate Success`)
            } catch (error) {
                reject(error)
            }
        })
    }


    static async downloadFileShare(fileName) {
        return new Promise(async (resolve, reject) => {
            try {
                const Env = await env()
                const { STORAGE_AZURE_CONNECTION_STRING, STORAGE_AZURE_STORAGE_NAME } = Env
                // connection Azure
                const serviceClient = ShareServiceClient.fromConnectionString(STORAGE_AZURE_CONNECTION_STRING)

                // check shareClient and directoryClient
                const shareClient = serviceClient.getShareClient(STORAGE_AZURE_STORAGE_NAME).getDirectoryClient("recordings")

                let rows
                if (fileName && fileName.indexOf('_') !== -1) {
                    rows = fileName.split('_')
                }
                const FileNames = moment(rows.shift()).format('YYYY-MM-DD')
                const directoryClient = shareClient.getDirectoryClient(FileNames).getFileClient(fileName)

                if (! await directoryClient.exists()) {
                    reject({ message: "Data Not Found" })
                }
                const downloadFileResponse = await directoryClient.download();

                // console.log(downloadFileResponse._response.headers)
                const result = await this.convertSteam(downloadFileResponse.readableStreamBody)
              
                // fs.writeFileSync(fileName, result)

                resolve(result)
            } catch (error) {
                reject(error)
            }
        })
    }

}

module.exports = Azure