const fs = require("fs");
const Promise = require('bluebird')
const env = require('../../helpers/env')
// const { STORAGE_LOCAL_FOLDER } = process.env
module.exports = async (fileName, file) => {
    return new Promise(async (resolve, reject) => {
        try {
            const Env = await env()
            const { STORAGE_LOCAL_FOLDER } = Env
            let result
            const path = `${STORAGE_LOCAL_FOLDER}${fileName}`
            if (fileName && fileName.indexOf('/') !== -1) {
                result = fileName.split('/')
                result.pop()
            }

            const dir = `${STORAGE_LOCAL_FOLDER}${result.join('/')}`
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir, {
                    recursive: true
                });
            }
            file
                .on('error', error => {
                    if (file.truncated)
                        // delete the truncated file
                        fs.unlinkSync(path);
                    reject(error);
                })
                .pipe(fs.createWriteStream(path))
                .on('error', error =>
                    reject(error)
                )
                .on('finish', () => {
                    console.log(path, "<=== Path Call Recording")
                    resolve({ path })
                }
                )
        } catch (error) {
            reject(error)
        }
    })
}