
const fs = require('fs');
const Promise = require('bluebird')

class fileSystem {

    static async readFile(data) {
        return new Promise(async (resolve, reject) => {
            try {
                fs.readFile(data, async (err, rows) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(rows.toString())
                })
            } catch (error) {
                reject(error)
            }
        })
    }

    static async writeFile(fileName, data) {
        return new Promise(async (resolve, reject) => {
            try {
                fs.writeFileSync(fileName, data)
                resolve("save file successfull")
            } catch (error) {
                reject(error)
            }
        })
    }

}


module.exports = fileSystem