const fs = require("fs");
const openpgp = require("openpgp");
const path = require("path");
const Promise = require('bluebird');
const { DECRYPT_PRIVATEKEY_PATH } = process.env
const privateKeyArmored = fs.readFileSync(DECRYPT_PRIVATEKEY_PATH, "utf-8");
const env = require('../helpers/env')
openpgp.config.allow_insecure_decryption_with_signing_keys = true;

module.exports = async (file) => {
    return new Promise(async (resolve, reject) => {
        try {
            const Env = await env()
            const { DECRYPT_PASSPHRASE } = Env
            const privateKey = (await openpgp.key.readArmored([privateKeyArmored])).keys[0];
            if (DECRYPT_PASSPHRASE.length > 0) {
                await privateKey.decrypt(DECRYPT_PASSPHRASE);
            }
            const result = await openpgp.decrypt({
                message: await openpgp.message.readArmored(file),
                privateKeys: privateKey,
                format: "binary",
            });
            resolve(result)
        } catch (error) {
            reject(error)
        }
    });
};
