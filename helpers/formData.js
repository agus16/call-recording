const Promise = require('bluebird')
const formidable = require('formidable');

module.exports = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            // const form = formidable({ multiples: true });
            const form = new formidable.IncomingForm()
            form.parse(data, ((err, fields, files) => {
                const dataRes = {
                    fields,
                    files
                }
                resolve(dataRes)
            }));
        } catch (error) {
            reject(error)
        }
    })
}